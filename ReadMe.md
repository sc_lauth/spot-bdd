Done with:
- spot-dev application in Openshift 
- SPOT.SQL file
- spot database name with gen$spot schema name
- Gitlab 

# Pre-requisites

You have generated a token to use the api 

# Database integration in Openshift

Login to
https://scale-portal-eu.service.cloud.local:8443/console/projects

Choose spot-dev if needed

Applications > Pods

Click to Terminal Tab. 

Place you in the /tmp directory

    cd /tmp

    curl --header "PRIVATE_TOKEN: <YOUR_TOKEN>" https://gitlab.com/api/v4/projects/16611651/repository/tree"


The results is a Json where you can find the sha of the file you need to curl

    curl -o spot.sql --header "PRIVATE_TOKEN: <YOUR_TOKEN>" "https://gitlab.com/api/v4/projects/16611651/repository/blobs/<SHA FILE>/raw"

Integrate data

    cat SPOT.SQL | psql -d spot

Then grant permission

    psql
    \connect spot
    GRANT USAGE ON SCHEMA gen$spot TO spot;
    GRANT SELECT, INSERT, UPDATE, DELETE ON ALL TABLES IN SCHEMA gen$spot TO spot;
    GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA gen$spot TO spot;
    GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA gen$spot TO spot;

That's all.