# Parameter
FICHIER_SQL="02INSERT_output.sql"

# article
# blockedsapdata
# docdirectory
# docdirectory_organism
# doctype
# documents
# execmonitoring
# execmonitoring_documents
# execmonitoring_maillog
# fieldmapping
# galycode
# mailconfig
# maillog
# omclmailinglist +
# omclmailinglist_galycode
# organism
# outputdatasap
# sampleplate
# sapimportrule
# site
# userrole
# users +
# users_userrole

cat $FICHIER_SQL | grep "INSERT INTO blockedsapdata" >> data/01data_blockedsapdata.sql
cat $FICHIER_SQL | grep "INSERT INTO doctype" >> data/02data_doctype.sql
cat $FICHIER_SQL | grep "INSERT INTO fieldmapping" >> data/03data_fieldmapping.sql
cat $FICHIER_SQL | grep "INSERT INTO galycode" >> data/04data_galycode.sql
cat $FICHIER_SQL | grep "INSERT INTO mailconfig" >> data/05data_mailconfig.sql
cat $FICHIER_SQL | grep "INSERT INTO maillog" >> data/06data_maillog.sql
cat $FICHIER_SQL | grep "INSERT INTO omclmailinglist" >> data/07data_omclmailinglist.sql
cat $FICHIER_SQL | grep "INSERT INTO organism" >> data/08data_organism.sql
cat $FICHIER_SQL | grep "INSERT INTO outputdatasap" >> data/09data_outputdatasap.sql
cat $FICHIER_SQL | grep "INSERT INTO sampleplate" >> data/10data_sampleplate.sql
cat $FICHIER_SQL | grep "INSERT INTO site" >> data/11data_site.sql
cat $FICHIER_SQL | grep "INSERT INTO userrole" >> data/12data_userrole.sql
cat $FICHIER_SQL | grep "INSERT INTO users" >> data/13data_users.sql
cat $FICHIER_SQL | grep "INSERT INTO article" >> data/14data_article.sql
cat $FICHIER_SQL | grep "INSERT INTO docdirectory " >> data/15data_docdirectory.sql

cp data/15data_docdirectory.sql data/16data_docdirectory.sql

sed -i -r "s/VALUES.\((.*),(.*),(.*),(.*),(.*),(.*),(.*),(.*),(.*)\).*/VALUES\(\1,\2,\3,null,\5,\6,\7,\8,\9\);/g" data/15data_docdirectory.sql

sed -i -r "s/(.*VALUES).\((.*),(.*),(.*),(.*),(.*),(.*),(.*),(.*),(.*)\).*/UPDATE docdirectory SET parent_id=\5 WHERE docdirectory_id=\2;/g" data/16data_docdirectory.sql

cat $FICHIER_SQL | grep "INSERT INTO docdirectory_organism" >> data/17data_docdirectory_organism.sql
cat $FICHIER_SQL | grep "INSERT INTO documents" >> data/18data_documents.sql
cat $FICHIER_SQL | grep "INSERT INTO execmonitoring" >> data/19data_execmonitoring.sql
cat $FICHIER_SQL | grep "INSERT INTO sapimportrule" >> data/20data_sapimportrule.sql

echo 'SET search_path = "gen$spot";' >> spot_data.sql
echo 'BEGIN;' >> spot_data.sql
echo 'SET search_path = "gen$spot";' >> spot_data.sql

cat data/*.sql >> spot_data.sql

echo 'COMMIT;' >> spot_data.sql